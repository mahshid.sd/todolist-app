# TodoApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.6 and [JSON-Server](https://github.com/typicode/json-server)

## Development client

First Run `npm install` and then Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Development client 

Start JSON Server by running `json-server --watch db.json`

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TodoCardComponent } from './Components/todo-card/todo-card.component';
import { TodoListComponent } from './Components/todo-list/todo-list.component';
import { AddCardComponent } from './Components/add-card/add-card.component';
import { DoneListComponent } from './Components/done-list/done-list.component';
import { EditCardComponent } from './Components/edit-card/edit-card.component';
import { CardDetailsComponent } from './Components/card-details/card-details.component';

@NgModule({
  declarations: [
    AppComponent,
    TodoCardComponent,
    TodoListComponent,
    AddCardComponent,
    DoneListComponent,
    EditCardComponent,
    CardDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatListModule,
    MatDividerModule,
    MatIconModule,
    MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

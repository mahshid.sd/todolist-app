import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { toDoCard } from '../Models/Card.Model';


@Injectable({
  providedIn: 'root'
})
export class todoCardService {

  constructor(public http: HttpClient) {}

  addCard(Card: toDoCard) {
    return this.http.post('http://localhost:3000/todo', Card);
  }

  getCards(): Observable<toDoCard[]> {
    return this.http.get<toDoCard[]>('http://localhost:3000/todo');
  }

  deleteCard(id: number) {
    return this.http.delete(`http://localhost:3000/todo/${id}`)
  }

  addToDone(card: toDoCard) {
    return this.http.post('http://localhost:3000/done', card);
  }

  getDoneCards(): Observable<toDoCard[]> {
    return this.http.get<toDoCard[]>('http://localhost:3000/done');
  }

  editeCard(id: number, card: toDoCard) {
    return this.http.put(`http://localhost:3000/todo/${id}`, card);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { toDoCard } from '../Models/Card.Model';

@Injectable({
  providedIn: 'root'
})
export class DoneCardsService {

  constructor(public http: HttpClient) { }

  addToDone(card: toDoCard) {
    return this.http.post('http://localhost:3000/done', card);
  }

  getDoneCards(): Observable<toDoCard[]> {
    return this.http.get<toDoCard[]>('http://localhost:3000/done');
  }

  deleteCard(id: number) {
    return this.http.delete(`http://localhost:3000/done/${id}`)
  }

  editeCard(id: number, card: toDoCard) {
    return this.http.put(`http://localhost:3000/done/${id}`, card);
  }
}

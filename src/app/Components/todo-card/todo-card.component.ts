import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { toDoCard } from 'src/app/Models/Card.Model';

import { todoCardService } from '../../Services/todo-cards.service';
import { DoneCardsService } from '../../Services/done-cards.service';
import { DataSharingService } from '../../Services/data-sharing.service';

import { EditCardComponent } from '../edit-card/edit-card.component';
import { CardDetailsComponent } from '../card-details/card-details.component';

@Component({
  selector: 'app-todo-card',
  templateUrl: './todo-card.component.html',
  styleUrls: ['./todo-card.component.scss']
})
export class TodoCardComponent implements OnInit {

  @Input() card: toDoCard;
  @Input() isToDo: boolean;

  doneCard: toDoCard;

  constructor(public todoCardService: todoCardService,public doneCardService: DoneCardsService, public dialog: MatDialog, public dataSharingService: DataSharingService) { }

  ngOnInit(): void {
  }

  onDeletedClick(id: number, event:Event) {
    if(this.isToDo) {
      this.todoCardService.deleteCard(id).subscribe(()=> this.dataSharingService.renderComponent.next(true));
    } else {
      this.doneCardService.deleteCard(id).subscribe(()=> this.dataSharingService.renderComponent.next(true));
    }

    event.stopPropagation();
  }

  onDoneClick(card: toDoCard, event: Event) {
    this.doneCard = {title:card.title, content: card.content}
    this.todoCardService.addToDone(this.doneCard).subscribe(()=> {
      this.onDeletedClick(card.id, event);
      this.shareData();
      this.shareData();
    });
    event.stopPropagation();
  }

  onEditClick(card: toDoCard, event: Event){
    const dialogRef = this.dialog.open(EditCardComponent, {
      data: {
        id: card.id,
        title: card.title,
        content: card.content,
        isToDo: this.isToDo
      }
    });
    event.stopPropagation();
  }

  onTitleClick(card: toDoCard) {
    const dialogRef = this.dialog.open(CardDetailsComponent, {
      data: {
        id: card.id,
        content: card.content,
      }
    });
  }

  shareData() {
    this.dataSharingService.renderComponent.next(true)
  }
}

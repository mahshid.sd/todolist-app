import { Component, OnInit } from '@angular/core';
import { toDoCard } from 'src/app/Models/Card.Model';

import { todoCardService } from '../../Services/todo-cards.service';
import { DataSharingService } from '../../Services/data-sharing.service';

@Component({
  selector: 'app-done-list',
  templateUrl: './done-list.component.html',
  styleUrls: ['./done-list.component.scss']
})
export class DoneListComponent implements OnInit {

  doneCards: toDoCard[];

  constructor(public todoCardService: todoCardService,  public dataSharingService: DataSharingService) {
      this.renderComponent()
   }

  ngOnInit(): void {
    this.getDoneList();
  }

  getDoneList() {
    this.todoCardService.getDoneCards().subscribe(data => {
      this.doneCards = data
    })
  }

  renderComponent() {
    this.dataSharingService.renderComponent.subscribe(() => {
      this.getDoneList();
    });
  }
}

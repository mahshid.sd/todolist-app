import { Component, OnInit } from '@angular/core';

import { todoCardService } from '../../Services/todo-cards.service';
import { DataSharingService } from '../../Services/data-sharing.service';

import { toDoCard } from 'src/app/Models/Card.Model';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {


  cards: toDoCard[]

  constructor(public todoCardService: todoCardService, public dataSharingService: DataSharingService) {
      this.renderComponent();
  }

  ngOnInit(): void {
    this.getCards();
  }

  getCards() {
    this.todoCardService.getCards().subscribe(data => {
      this.cards = data;
    })
  }

  renderComponent() {
    this.dataSharingService.renderComponent.subscribe(() => {
      this.getCards();
    });
  }
}

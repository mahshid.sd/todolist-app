import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { toDoCard } from 'src/app/Models/Card.Model';

import { todoCardService } from '../../Services/todo-cards.service';
import { DoneCardsService } from '../../Services/done-cards.service';
import { DataSharingService} from '../../Services/data-sharing.service';

@Component({
  selector: 'app-edit-card',
  templateUrl: './edit-card.component.html',
  styleUrls: ['./edit-card.component.scss']
})
export class EditCardComponent implements OnInit {

  editForm = new FormGroup({
    title: new FormControl(this.data.title),
    content: new FormControl(this.data.content),
  });
  edited: toDoCard;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public todoCardService: todoCardService, public doneCardService:DoneCardsService, public dataSharingService: DataSharingService) { }

  ngOnInit(): void {
  }

  onEditClick() {
    if(this.data.isToDo)
    {
      this.todoCardService.editeCard(this.data.id, this.editForm.value).subscribe(()=> this.shareData());
    } else {
      this.doneCardService.editeCard(this.data.id, this.editForm.value).subscribe(()=> this.shareData());
    }
  }

  shareData() {
    this.dataSharingService.renderComponent.next(true)
  }
}

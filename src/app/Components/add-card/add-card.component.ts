import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { todoCardService } from '../../Services/todo-cards.service';
import { DataSharingService } from '../../Services/data-sharing.service';


@Component({
  selector: 'app-add-card',
  templateUrl: './add-card.component.html',
  styleUrls: ['./add-card.component.scss']
})
export class AddCardComponent implements OnInit {

  toDoForm = new FormGroup({
    title: new FormControl(''),
    content: new FormControl(''),
  });

  constructor(public todoCardService: todoCardService, public router: Router, public dataSharingService: DataSharingService) { }

  ngOnInit(): void {
  }

  onAddClick() {
    this.todoCardService.addCard(this.toDoForm.value).subscribe(()=>
    this.dataSharingService.renderComponent.next(true)
    );
    this.toDoForm.reset()
  }
}

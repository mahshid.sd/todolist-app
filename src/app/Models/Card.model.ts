export interface toDoCard {
  id?: number,
  title: string,
  content: string
}
